## Instal·leu Vagrant ##

A la pàgina [corresponent](http://www.vagrantup.com/), aneu a **Downloads** i
baixeu-vos l'aplicació del vostre sistema operatiu.

És possible que això us exigeixi instal·lar també [VirtualBox](https://www.virtualbox.org/).


## Engegueu la màquina virtual ##

Cal que tingueu connexió internet i que estigueu disposats a tenir l'ordinador engegat durant hores.
Feu
```
#!sh
vagrant up
```
i espereu una llaaaaaaarga estona. Si al final us dóna error proveu de fer:
```
#!sh
vagrant provision
```
